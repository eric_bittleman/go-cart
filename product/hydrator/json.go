package hydrator

import (
	"encoding/json"
	"io"

	"code.bittles.net/ebittleman/go-cart/product/api"
)

type ProductJsonHydrator struct{}

func (p *ProductJsonHydrator) Extract(product api.Product, writer io.Writer) error {
	encoder := json.NewEncoder(writer)
	return encoder.Encode(product)
}

func (p *ProductJsonHydrator) Hydrate(reader io.Reader, product api.Product) error {
	decoder := json.NewDecoder(reader)
	return decoder.Decode(product)
}
