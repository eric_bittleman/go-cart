package persistence

import (
	"fmt"
	"os"
	"path/filepath"

	core "code.bittles.net/ebittleman/go-cart/core/api"
	"code.bittles.net/ebittleman/go-cart/product/api"
	"code.bittles.net/ebittleman/go-cart/product/model"
)

// Product
// ====================================

type ProductFileManager struct {
	Hydrator         api.ProductHydrator
	VersionGenerator core.VersionGenerator
	Directory        string

	fs core.FileSystem
}

func NewProductFileManager(
	directory string,
	hydrator api.ProductHydrator,
	versionGenerator core.VersionGenerator,
	fs core.FileSystem,
) api.ProductObjectManger {
	manager := &ProductFileManager{
		Hydrator:         hydrator,
		VersionGenerator: versionGenerator,
		Directory:        directory,
		fs:               fs,
	}

	manager.fs.CreateDirectory(manager.Directory)

	return manager
}

func (manager *ProductFileManager) getFileName(id string) string {
	return filepath.Join(manager.Directory, "product_"+id+".json")
}

func (manager *ProductFileManager) Find(id string) (api.Product, error) {
	return manager.findByFilename(manager.getFileName(id))
}

func (manager *ProductFileManager) FindAll() ([]api.Product, error) {
	filenames, err := manager.fs.ListFilenames(manager.Directory)

	if err != nil {
		return nil, err
	}

	products := make([]api.Product, 0, 10)

	for _, filename := range filenames {
		product, err := manager.findByFilename(
			filepath.Join(manager.Directory, filename),
		)

		if err != nil {
			return nil, err
		}

		products = append(products, product)
	}

	return products, nil
}

func (manager *ProductFileManager) findByFilename(filename string) (api.Product, error) {
	product := model.NewProductPrototype()

	reader, err := manager.fs.GetReader(filename)

	if err != nil {
		return nil, err
	}

	defer reader.Close()

	err = manager.Hydrator.Hydrate(reader, product)

	if err != nil {
		return nil, err
	}

	return product, nil
}

func (manager *ProductFileManager) Persist(product api.Product) error {

	idInst := product.GetId()

	id, ok := idInst.(string)

	if !ok {
		return fmt.Errorf("Invalid Product Id: %v", idInst)
	}

	if !manager.IsVersionValid(product) {
		return fmt.Errorf("Product Collision Decteted")
	}

	product.SetVersion(manager.VersionGenerator(product))

	writer, err := manager.fs.GetWriter(manager.getFileName(id))

	if err != nil {
		return err
	}

	defer writer.Close()

	return manager.Hydrator.Extract(product, writer)
}

func (manager *ProductFileManager) IsVersionValid(product api.Product) bool {
	currentProduct, err := manager.Find(product.GetId().(string))

	if os.IsNotExist(err) {
		return true
	}

	if err != nil {
		return false
	}

	return currentProduct.GetVersion() != product.GetVersion()
}
