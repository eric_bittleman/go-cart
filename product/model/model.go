package model

import (
	"encoding/json"
	"fmt"
	"reflect"
	"sort"

	core "code.bittles.net/ebittleman/go-cart/core/api"
	coreModel "code.bittles.net/ebittleman/go-cart/core/model"
	"code.bittles.net/ebittleman/go-cart/product/api"
)

// Product
// ============================

type productV1 struct {
	Id      string `json:"id"`
	Version string `json:"version"`
	Name    string `json:"name"`

	Type string `json:"type"`

	Price coreModel.Money `json:"price"`

	MimeType string `json:"mime-type"`
}

func NewProduct(
	name string,
	productType string,
	amount int,
	currency string,
	productTypeSpec core.StringSpecification,
) (api.Product, error) {

	// validate ProductType as per provided specification
	err := productTypeSpec(productType)

	if err != nil {
		return nil, err
	}

	// create a new money value using the core model
	money, err := coreModel.NewMoney(amount, currency)

	if err != nil {
		return nil, err
	}

	// all "new" work done, grab an instance
	return &productV1{
		Id:       "",
		Version:  "",
		Name:     name,
		Type:     productType,
		Price:    money,
		MimeType: api.MimeTypeV1,
	}, nil
}

func (p *productV1) GetId() interface{} {
	return p.Id
}

func (p *productV1) SetId(id interface{}) error {
	newId, ok := id.(string)

	if !ok {
		return fmt.Errorf("Invalid Id Type: %s", reflect.TypeOf(id))
	}

	p.Id = newId

	return nil
}

func (p *productV1) GetVersion() string {
	return p.Version
}

func (p *productV1) SetVersion(version string) {
	p.Version = version
}

func (p *productV1) GetType() string {
	return p.Type
}

func (p *productV1) Clone() api.Product {
	return &productV1{
		Id:    "",
		Name:  p.Name,
		Type:  p.Type,
		Price: p.Price.Clone(),
	}
}

func (p *productV1) String() string {
	data, _ := json.Marshal(p)
	return string(data)
}

func NewProductPrototype() api.Product {
	return &productV1{}
}

func NewValidTypeSpec(validTypes []string) core.StringSpecification {
	numTypes := len(validTypes)

	validTypesSort := sort.StringSlice(validTypes)
	validTypesSort.Sort()

	return func(value string) error {
		typeIndex := validTypesSort.Search(value)

		if typeIndex == numTypes || value != validTypesSort[typeIndex] {
			return fmt.Errorf("Invalid Value: %s", value)
		}

		return nil
	}
}
