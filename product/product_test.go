package product_test

import (
	"fmt"
	"log"
	"os"
	"testing"

	"code.bittles.net/badger/command"
	"code.bittles.net/badger/events"
	core "code.bittles.net/ebittleman/go-cart/core/api"
	"code.bittles.net/ebittleman/go-cart/product"
)

func TestCommandNewProduct(t *testing.T) {

	bus, eventManager := initBus(t)
	var expected = `&{{"id":"47b2888d-18bc-4d36-8eb3-6da6e4a31430","version":"47b2888d-18bc-4d36-8eb3-6da6e4a31430","name":"Test Product","type":"donation","price":{"amount":1000,"currency":"USD"},"mime-type":"application/vnd.gocart.product.v1"}}`
	var actual string

	eventManager.Attach(
		product.EVENT_PRODUCT_CREATED,
		func(event *events.Event) (interface{}, error) {
			payload, _ := event.Values["payload"]
			actual = fmt.Sprintf("%s", payload)
			return nil, nil
		},
		0,
	)

	cmd := &product.NewProductCommand{
		Version:  "v1",
		Name:     "Test Product",
		Type:     "donation",
		Amount:   1000,
		Currency: "USD",
	}

	_, err := bus.Execute(cmd)

	if err != nil {
		t.Fatal(err)
	}

	bus.FlushEvents()

	if expected != actual {
		t.Log(actual)
		t.Log(expected)
		t.Fatal("Error Creating Product")
	}

}

func TestQuery(t *testing.T) {
	bus, _ := initBus(t)
	query := &product.ListProductQuery{}

	results, err := bus.Query(query)

	if err != nil {
		t.Fatal(err)
	}

	t.Log(results)
}

func TestTearDown(t *testing.T) {
	os.RemoveAll("./products")
}

func initBus(t *testing.T) (command.CommandBus, events.EventManger) {
	eventManager := events.New()

	bus := command.NewCommandBus(eventManager)

	service := product.NewJsonFileProductService(
		"./products",
		func() string {
			return "47b2888d-18bc-4d36-8eb3-6da6e4a31430"
		},
		func(entity core.VersionableEntity) string {
			return entity.GetId().(string)
		},
		[]string{
			"physical",
			"donation",
			"digital",
		},
		log.New(os.Stderr, "", log.LstdFlags),
	)

	handler := product.NewNewProductHandler(bus, service)

	bus.RegisterHandler(&product.NewProductCommand{}, handler.Handle)

	querier := product.NewListProductQuery(bus, service)

	bus.RegisterQuerier(&product.ListProductQuery{}, querier.Query)

	return bus, eventManager
}
