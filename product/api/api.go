package api

import (
	"io"

	core "code.bittles.net/ebittleman/go-cart/core/api"
)

// Product

const MimeTypeV1 = "application/vnd.gocart.product.v1"

type Product interface {
	core.VersionableEntity
	GetType() string
	Clone() Product
}

type ProductHydrator interface {
	Extract(Product, io.Writer) error
	Hydrate(io.Reader, Product) error
}

type ProductObjectManger interface {
	Find(id string) (Product, error)
	FindAll() ([]Product, error)
	Persist(Product) error
}

type ProductRepository interface {
	NextProductId() string
	FindById(id string) Product
	FindByType(productType string) []Product
	FindAll() []Product
	AddProduct(Product) error
}

type ProductFactory interface {
	CreateFromNewProductRequest(
		req *NewProductRequest,
	) (Product, error)
}

type NewProductRequest struct {
	Name     string
	Type     string
	Amount   int
	Currency string
}

type NewProductResponse struct {
	Product Product
	Error   error
}

type ProductService interface {
	NewProduct(*NewProductRequest) (Product, error)
	ListAllProducts() []Product
}
