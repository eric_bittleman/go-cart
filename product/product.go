package product

import (
	"log"

	core "code.bittles.net/ebittleman/go-cart/core/api"
	corePersistence "code.bittles.net/ebittleman/go-cart/core/persistence"
	"code.bittles.net/ebittleman/go-cart/product/api"
	"code.bittles.net/ebittleman/go-cart/product/hydrator"
	"code.bittles.net/ebittleman/go-cart/product/model"
	"code.bittles.net/ebittleman/go-cart/product/persistence"
)

// Implementation
//=======================================

func NewJsonFileProductService(
	directory string,
	UUIdGenerator core.StringIdGenerator,
	VersionGenerator core.VersionGenerator,
	validTypes []string,
	log *log.Logger,
) api.ProductService {

	service := &ProductDefaultService{

		Factory: NewValidProductTypeFactory(validTypes),

		Repo: NewJsonProductFileRepository(directory, UUIdGenerator, VersionGenerator, log),
	}

	return service
}

func NewValidProductTypeFactory(validTypes []string) api.ProductFactory {

	return &ProductDefaultFactory{
		ProductTypeSpec: model.NewValidTypeSpec(
			validTypes,
		),
	}
}

func NewJsonProductFileRepository(
	directory string,
	UUIdGenerator core.StringIdGenerator,
	VersionGenerator core.VersionGenerator,
	log *log.Logger,
) api.ProductRepository {

	return &ProductDefaultRepository{

		ProductIdGenerator: UUIdGenerator,

		Manager: persistence.NewProductFileManager(
			directory,
			&hydrator.ProductJsonHydrator{},
			VersionGenerator,
			&corePersistence.BasicFileSystem{},
		),

		Log: log,
	}
}
