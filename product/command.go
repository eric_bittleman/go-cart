package product

import (
	"fmt"
	"reflect"

	"code.bittles.net/badger/command"
	"code.bittles.net/ebittleman/go-cart/product/api"
)

const (
	EVENT_PRODUCT_CREATED = "product::created"
)

type NewProductEvent struct {
	Product api.Product
}

func (e *NewProductEvent) GetType() string {
	return EVENT_PRODUCT_CREATED
}

type NewProductCommand struct {
	Version  string
	Name     string
	Type     string
	Amount   int
	Currency string
}

type NewProductHandler struct {
	bus     command.CommandBus
	service api.ProductService
}

func NewNewProductHandler(bus command.CommandBus, service api.ProductService) command.Handler {
	return &NewProductHandler{bus: bus, service: service}
}

func (ph *NewProductHandler) Handle(cmd command.Command) (interface{}, error) {
	productCmd, ok := cmd.(*NewProductCommand)

	if !ok {
		return nil, fmt.Errorf("Invalid Command Type: %s", reflect.TypeOf(cmd))
	}

	req := &api.NewProductRequest{
		Name:     productCmd.Name,
		Type:     productCmd.Type,
		Amount:   productCmd.Amount,
		Currency: productCmd.Currency,
	}

	product, err := ph.service.NewProduct(req)

	if err != nil {
		return nil, err
	}

	ph.bus.Raise(&NewProductEvent{
		Product: product,
	})

	return product, err
}

type ListProductQuery struct{}

type ListProductQuerier struct {
	bus     command.CommandBus
	service api.ProductService
}

func NewListProductQuery(bus command.CommandBus, service api.ProductService) command.Querier {
	return &ListProductQuerier{
		bus:     bus,
		service: service,
	}
}

func (querier *ListProductQuerier) Query(q command.Query) (interface{}, error) {

	products := querier.service.ListAllProducts()

	return products, nil
}
