package product

import (
	core "code.bittles.net/ebittleman/go-cart/core/api"
	"code.bittles.net/ebittleman/go-cart/product/api"
	"code.bittles.net/ebittleman/go-cart/product/model"
)

type ProductDefaultFactory struct {
	ProductTypeSpec core.StringSpecification
}

func (factory *ProductDefaultFactory) CreateFromNewProductRequest(
	req *api.NewProductRequest,
) (api.Product, error) {

	return model.NewProduct(
		req.Name,
		req.Type,
		req.Amount,
		req.Currency,
		factory.ProductTypeSpec,
	)
}
