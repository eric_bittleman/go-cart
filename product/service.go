package product

import "code.bittles.net/ebittleman/go-cart/product/api"

type ProductDefaultService struct {
	Factory api.ProductFactory
	Repo    api.ProductRepository
}

func (service *ProductDefaultService) NewProduct(req *api.NewProductRequest) (api.Product, error) {
	product, err := service.Factory.CreateFromNewProductRequest(
		req,
	)

	if err != nil {
		return nil, err
	}

	err = service.Repo.AddProduct(product)

	if err != nil {
		return nil, err
	}

	return product, nil
}

func (service *ProductDefaultService) ListAllProducts() []api.Product {
	return service.Repo.FindAll()
}
