package product

import (
	"log"

	core "code.bittles.net/ebittleman/go-cart/core/api"
	"code.bittles.net/ebittleman/go-cart/product/api"
	"code.google.com/p/go-uuid/uuid"
)

func ProductIdUUIDStrategy() string {
	return uuid.NewRandom().String()
}

type ProductDefaultRepository struct {
	Manager            api.ProductObjectManger
	ProductIdGenerator core.StringIdGenerator
	Log                *log.Logger
}

func (repo *ProductDefaultRepository) NextProductId() string {
	return repo.ProductIdGenerator()
}

func (repo *ProductDefaultRepository) FindById(id string) api.Product {
	product, err := repo.Manager.Find(id)

	if err != nil {
		repo.Log.Println(err)
	}

	return product
}

func (repo *ProductDefaultRepository) FindByType(productType string) []api.Product {
	allProducts, err := repo.Manager.FindAll()

	if err != nil {
		return []api.Product{}
	}

	products := make([]api.Product, 0, 10)

	for _, product := range allProducts {

		if err != nil || product.GetType() != productType {
			continue
		}

		products = append(products, product)
	}

	return products
}

func (repo *ProductDefaultRepository) FindAll() []api.Product {
	allProducts, err := repo.Manager.FindAll()

	if err != nil {
		repo.Log.Println(err)
	}

	return allProducts
}

func (repo *ProductDefaultRepository) UpdateProduct(product api.Product) error {
	return repo.Manager.Persist(product)
}

func (repo *ProductDefaultRepository) AddProduct(product api.Product) error {
	product.SetId(repo.NextProductId())

	return repo.Manager.Persist(product)
}
