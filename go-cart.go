package gocart

import (
	"fmt"
	"net/http"
	"reflect"
)

// This file is nonsense, but a kind of cool idea. ignore it for now
//========================================================

type NewStatusCommand struct {
	Message string
}

type NewStatusHandler struct {
}

func (n *NewStatusHandler) Handle(cmd Command) error {

	_, ok := cmd.(*NewStatusCommand)

	if !ok {
		return fmt.Errorf("Incorrect Command Type %s", reflect.TypeOf(cmd))
	}

	return nil
}

type Status struct {
	Message string
}

type StatusHandler func(chan Status) chan Status

func compose(f, g StatusHandler) StatusHandler {
	return func(x chan Status) chan Status {
		return f(g(x))
	}
}

func RequestHandler(resp http.ResponseWriter, req *http.Request) {
	status := Status{Message: req.FormValue("message")}
	in := make(chan Status, 1)
	in <- status
	handler := GetStatusHandler(GlobalDbConn)
	out := handler(in)

	resp.Write(<-out)
}

func GetStatusHandler(db *Connection) StatusHandler {
	return compose(
		WriteStatus(db),
		ValidateStatus,
	)
}

func ValidateStatus(in chan Status) chan Status {
	out := make(chan Status)
	go func() {
		status := <-in
		if len(status.Message) < 1 {
			close(out)
		}

		out <- status
	}()
	return out
}

func WriteStatus(db *Connection) StatusHandler {
	return func(in chan Status) chan Status {
		out := make(chan Status)
		go func() {
			err := db.Insert(status)

			if err != nil {
				close(out)
			}

			out <- status
		}()
		return out
	}
}
