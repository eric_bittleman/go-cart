package api

import (
	"io"
	"net/http"
)

// API
//=======================================

// Core
type Event interface {
	GetType() string
}

type EventHandler interface {
	Handle(Event) error
}

type Context interface {
	HandleEvent(string, EventHandler)
	HandleHttpRequest(string, http.Handler)
}

type Module interface {
	Register(Context)
}

type MoneyValue interface {
	Clone() MoneyValue
}

type Entity interface {
	GetId() interface{}
	SetId(id interface{}) error
}

type Versionable interface {
	GetVersion() string
	SetVersion(string)
}

type VersionableEntity interface {
	Entity
	Versionable
}

type FileSystem interface {
	DirectoryCreator
	DirectoryLister
	ReaderGetter
	WriterGetter
}

type DirectoryCreator interface {
	CreateDirectory(directory string) error
}

type DirectoryLister interface {
	ListFilenames(directory string) ([]string, error)
}

type ReaderGetter interface {
	GetReader(key string) (io.ReadCloser, error)
}

type WriterGetter interface {
	GetWriter(key string) (io.WriteCloser, error)
}

type StringSpecification func(string) error

type StringIdGenerator func() string

type VersionGenerator func(VersionableEntity) string
