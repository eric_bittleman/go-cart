package model

type Money struct {
	Amount   int    `json:"amount"`
	Currency string `json:"currency"`
}

func NewMoney(amount int, currency string) (Money, error) {
	return Money{
		Amount:   amount,
		Currency: currency,
	}, nil
}

func (m Money) Clone() Money {
	clone, _ := NewMoney(m.Amount, m.Currency)
	return clone
}
