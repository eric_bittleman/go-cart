package persistence

import (
	"io"
	"io/ioutil"
	"os"
)

type BasicFileSystem struct{}

func (fs *BasicFileSystem) CreateDirectory(directory string) error {
	return os.MkdirAll(directory, 0775)
}

func (fs *BasicFileSystem) ListFilenames(directory string) ([]string, error) {
	fileinfos, err := ioutil.ReadDir(directory)

	if err != nil {
		return nil, err
	}

	files := make([]string, len(fileinfos))
	for i, fileinfo := range fileinfos {
		files[i] = fileinfo.Name()
	}

	return files, nil
}

func (fs *BasicFileSystem) GetReader(key string) (io.ReadCloser, error) {
	return os.Open(key)
}

func (fs *BasicFileSystem) GetWriter(key string) (io.WriteCloser, error) {
	return os.OpenFile(key, os.O_RDWR|os.O_TRUNC|os.O_CREATE, 0775)
}
